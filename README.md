# AGH Pracownia Problemowa

Badanie zostały przeprowadzone w ramach Pracowni Problemowej, a następnie zostały kontynuowane w ramach pracy magisterskiej.

Repozytorium zawiera pliki:

1.  ./dane/Rp - są to dane dotyczące granicy plastyczności
2. ./dane/Rm - są to dane dotyczące wytrzymałości na rozciąganie
3. ./dane/cut_structures - dane podzielone na 7 klas zgodnie z normą ze względu na analizę wizualną
4. ./dane/cut_big_structures - dane zawierające tylko większe z powyższych struktur, w celu sprawdzenia wpływu takich danych na wyniki

We wszystkich tych zbiorach obowiązuje następująca terminologia:
* raw_data - nieprzetworzone dane
* proc_data - przetworzone dane zgodnie z opisem w raporcie
